package taller.mundo;


import java.util.ArrayList;

import JSci.maths.statistics.NormalDistribution;
import taller.estructuras.NodoHash;
import taller.estructuras.TablaHash;

public class Directorio {
	
	private ArrayList<Ciudadano> listaCiudadanos;
	private TablaHash<Llave, Ciudadano> tablaCiudadanos; 
	
	public Directorio (){
		listaCiudadanos = new ArrayList<Ciudadano>();
		tablaCiudadanos = new TablaHash<>();
	}

	public void agregarCiudadano(String pNombre, String pApellido,int pTelefono,double pLatitud,double pLongitud){
		
		Llave nuevaLlave = new Llave<>(pTelefono);
		String pLocalizacion = ""+pLatitud+"&"+pLongitud;
		Ciudadano nuevo = new Ciudadano(pNombre, pApellido, pLocalizacion, pTelefono);
		NodoHash<Llave, Ciudadano> nuevoCiudadano = new NodoHash<Llave, Ciudadano>(nuevaLlave, nuevo);
		listaCiudadanos.add(nuevo);               
		tablaCiudadanos.put(nuevaLlave, nuevo);
		
	}
	
	public TablaHash getTabla(){
		return tablaCiudadanos; 
	}
	public Ciudadano buscarCiudadanoTelefono(Llave tel){
		return tablaCiudadanos.get(tel);
	}
}
