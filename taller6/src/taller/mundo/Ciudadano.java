package taller.mundo;

public class Ciudadano {

	private String nombre;
	private String apellido;
	private String localizacion;
	private int telefono;
	
	public Ciudadano(String pNombre, String pApellido, String pLocalizacion,int pTelefono ){
		nombre = pNombre;
		apellido = pApellido;
		localizacion = pLocalizacion;
		telefono = pTelefono;
    	}
	
	public String darNombre(){
		return nombre;
	}
	public String darApellido(){
		return apellido;
	}
	public String darLocalizacion(){
		return localizacion;
	}
	public int darTelefono(){
		return telefono;
	}

}
