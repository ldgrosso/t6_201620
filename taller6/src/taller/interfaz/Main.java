package taller.interfaz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import JSci.maths.statistics.NormalDistribution;
import JSci.maths.statistics.ProbabilityDistribution;
import taller.mundo.Directorio;
import taller.mundo.Llave;


public class Main {

	private static String rutaEntrada = "./data/ciudadLondres.csv";
	private static Directorio directorio;

	public static void main(String[] args) {
		directorio = new Directorio();
		//Cargar registros
		System.out.println("Bienvenido al directorio de emergencias por colicsiones de la ciudad de Londres");
		System.out.println("Espere un momento mientras cargamos la información...");
		System.out.println("Esto puede tardar unos minutos...");
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaEntrada)));
			String entrada = br.readLine();
			NormalDistribution n = new NormalDistribution(0, 1);
			System.out.println(n.getMean());
			System.out.println(n.getVariance());
			System.out.println(n.cumulative(0.05));
			System.out.println(n.inverse(0.05));
			//TODO: Inicialice el directorio t
			int i = 0;
			entrada = br.readLine();
			while (entrada != null){
				String[] datos = entrada.split(",");
				String pNombre = datos[0];
				String pApellido = datos[1];
				int pTelefono= Integer.valueOf(datos[2]);
				double pLatitud = Double.parseDouble(datos[3]);
				double pLongitud = Double.parseDouble(datos[3]);
				directorio.agregarCiudadano(pNombre, pApellido, pTelefono, pLatitud, pLongitud);
				//TODO: Agrege los datos al directorio de emergencias
				//Recuerde revisar en el enunciado la estructura de la información
				++i;
				if (++i%500000 == 0)
					System.out.println(i+" entradas...");

				entrada = br.readLine();
			}
			System.out.println(i+" entradas cargadas en total");
			br.close();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Directorio de emergencias por colisiones de Londres v1.0");

		boolean seguir = true;

		while (seguir)
			try {
				System.out.println("Bienvenido, seleccione alguna opcion del menú a continuación:");
				System.out.println("1: Agregar ciudadano a la lista de emergencia");
				System.out.println("2: Buscar ciudadano por número de contacto del acudiante");
				System.out.println("3: Buscar ciudadano por apellido");
				System.out.println("4: Buscar ciudadano por su locaclización actual");
				System.out.println("Exit: Salir de la aplicación");
				
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String in = br.readLine();
				switch (in) {
				case "1":
					System.out.println("Escriba el nombre ");
					String nombre = br.readLine();
					System.out.println("Escriba el apellido");
					String apellido = br.readLine();
					System.out.println("Escriba el telefono");
					int telefono = Integer.parseInt(br.readLine());
					System.out.println("Escriba la latitud");
					double latitud= Double.parseDouble(br.readLine());
					System.out.println("Escriba la longitud");
					double longitud= Double.parseDouble(br.readLine());
					directorio.agregarCiudadano(nombre, apellido, telefono, latitud, longitud);
					//TODO: Implemente el requerimiento 
					//Agregar ciudadano a la lista de emergencia
					break;
				case "2":
					System.out.println("Ingrese el telefono del acudiente: ");
					String tell = br.readLine();
					Llave tel = new Llave<>(tell);
					directorio.buscarCiudadanoTelefono(tel);
					//TODO: Implemente el requerimiento 2
					//Buscar un ciudadano por el número de teléfono de su acudiente
					break;
				case "3":
					//TODO: Implemente el requerimiento 3
					//Buscar ciudadano por apellido/nombre
					break;
				case "4":
					//TODO: Implemente el requerimiento 4
					//Buscar ciudadano por su locaclización actual
					break;
				case "Exit":
					System.out.println("Cerrando directorio...");
					seguir = false;
					break;

				default:
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}


	}

}
