package taller.estructuras;

import java.util.ArrayList;

public class TablaHash<K extends Comparable<K> ,V> {

	private static final String LP = "LP";
	private static final String SC = "SC";
	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;


	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	private NodoHash<K, V>[] tabla;
	
	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;
	
	public NodoHash[] getTabla(){
		return tabla;
	}

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash(){
		tabla = new NodoHash[1000001];
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad, float factorCargaMax) {
		tabla = new NodoHash[1000001];
		//TODO: Inicialice la tabla con los valores dados por parametro

	}

	public void put(K llave, V valor){
		int posicion = hash(llave);
		NodoHash<K, V> nuevoNodo = new NodoHash<K, V>(llave, valor);
		if(tabla[posicion]!=null){
			int i = 0;
			while (posicion+i<=tabla.length && tabla[posicion+i-1]!=null){
				i ++;
			}
			if(posicion+i==tabla.length+1){
				posicion = 0;
				i = 0 ;
				while (posicion+i<tabla.length && tabla[posicion+i]!=null){
					i ++;
				}
			}
			tabla[posicion+i]= nuevoNodo;
		}
		else{
			tabla[posicion] =  nuevoNodo;
		}
		
		System.out.println(posicion);
	}

	public V get(K llave){
		boolean encontro = false;
		int posicion = hash(llave);
		while(!encontro){
			if(tabla[posicion].getLlave().equals(llave)){
				encontro = true;
			}
			else{
				posicion ++;
			}	
		}
		V valor = tabla[posicion].getValor();

		return valor;
	}

	public V delete(K llave){
		int posicion = hash(llave);
		V valor = tabla[posicion].getValor();
		tabla[posicion] = null;

		return valor;
	}

	//Hash
	private int hash(K llave)
	{
		int a = tabla.length;
		int indice = (llave.hashCode()%a)-1;
				
		return indice>=0?indice:indice+500;
	}
	
		//TODO: Permita que la tabla sea dinamica

}