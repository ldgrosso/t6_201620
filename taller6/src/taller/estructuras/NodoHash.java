package taller.estructuras;

import taller.mundo.INodo;

public class NodoHash<K,V> implements INodo<K,V>{

	private K llave;
	private V valor;
	
	public NodoHash(K llave, V valor) {
		super();
		this.llave = llave;
		this.valor = valor;
	}
	
	public K getLlave() {
		return llave;
	}
	
	public void setLlave(K llave) {
		this.llave = llave;
	}
	
	public V getValor() {
		return valor;
	}
	
	public void setValor(V valor) {
		this.valor = valor;
	}

	@Override
	public int compareTo(NodoHash<K, V> o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
